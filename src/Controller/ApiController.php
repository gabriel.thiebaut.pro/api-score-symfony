<?php

namespace App\Controller;

use App\Entity\Scores;
use App\Repository\ScoresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api', name: 'api')]
class ApiController extends AbstractController
{
    // This route (api/scores) shows all player scores from the database
    #[Route('/scores', name:'scores')]
    public function scores(ScoresRepository $scoreRepo): Response
    {
        return $this->json($scoreRepo->findAll(), 200, []);
    }

    // This route (api/scores/{id}) shows scores of one player from the database with GET method
    #[Route('/scores/{id}', name: 'scores_id', methods: 'GET')]
    public function scoresShow(ScoresRepository $scoreRepo, int $id)
    {
        return $this->json($scoreRepo->findBy(array('id' => $id)), 200, []);
    }

    // This route (api/scores/add) adds scores to the database with POST method
    #[Route('/scores/add', name:'scores_add', methods:'POST')]
    public function scoresAdd(Request $request, SerializerInterface $serializer)
    {
        // Decode datas and deserialize data from POST method
        $datas = $request->getContent();
        $scores = $serializer->deserialize($datas, Scores::class, 'json');

        // Save to database
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($scores);
        $entityManager->flush();

        // Return http status 200 if ok
        return new Response('OK', 201);
        // Return http status 404 if not ok
        return new Response('NOT OK', 404);
    }
}
